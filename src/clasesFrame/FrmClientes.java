
package clasesFrame;

import CatalogosCRUD.*;
import bestbuy.*;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import login.Usuario;

/**
 *
 * @author JUAN-PC
 */
public final class FrmClientes extends javax.swing.JFrame {

    LogicaClientes log;
    private final DefaultTableModel model;

    private Usuario login;
    /**
     * Creates new form FrmClientes
     */
    public FrmClientes() {
        initComponents();
        setLocationRelativeTo(null);
        login = new Usuario();
        log = new LogicaClientes();
        model = (DefaultTableModel) tbClientes.getModel();
        cargarTabla();
    }
    public FrmClientes(Usuario login) {
        initComponents();
        setLocationRelativeTo(null);
         this.login = login;
        log = new LogicaClientes();
        model = (DefaultTableModel) tbClientes.getModel();
        cargarTabla();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnRegresar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtCedula = new javax.swing.JTextField();
        txtTelefono = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbClientes = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        btnEditarCliente = new javax.swing.JButton();
        btnEliminarCliente = new javax.swing.JButton();
        btnRegistrar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnRegresar.setText("<< Regresar");
        btnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegresarActionPerformed(evt);
            }
        });
        getContentPane().add(btnRegresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 103, -1));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Nombre");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(66, 115, -1, 14));

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Cedula");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 154, -1, -1));

        jLabel3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Telefono");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(59, 192, -1, -1));
        getContentPane().add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(149, 113, 172, -1));
        getContentPane().add(txtCedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(149, 153, 172, -1));
        getContentPane().add(txtTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(149, 191, 172, -1));

        jLabel5.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Registrar Clientes");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(133, 47, -1, -1));

        tbClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre", "Cedula", "Telefono"
            }
        ));
        tbClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbClientesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbClientes);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(327, 82, -1, 167));

        jLabel6.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel6.setText("Clientes");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(512, 47, -1, -1));

        btnEditarCliente.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnEditarCliente.setText("Editar Cliente");
        btnEditarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarClienteActionPerformed(evt);
            }
        });
        getContentPane().add(btnEditarCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 270, -1, -1));

        btnEliminarCliente.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnEliminarCliente.setText("Eiminar Cliente");
        btnEliminarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarClienteActionPerformed(evt);
            }
        });
        getContentPane().add(btnEliminarCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 270, -1, -1));

        btnRegistrar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistroActionPerformed(evt);
            }
        });
        getContentPane().add(btnRegistrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(191, 286, 98, -1));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/clientes.jpg"))); // NOI18N
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 860, 360));

        pack();
    }// </editor-fold>//GEN-END:initComponents

  

                                               

    private void btnCargarClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarClientesActionPerformed
   
    }//GEN-LAST:event_btnCargarClientesActionPerformed

    private void tbClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbClientesMouseClicked
        int row = tbClientes.getSelectedRow();
        if (row >= 0) {
            try {
                txtNombre.setText(tbClientes.getValueAt(row, 0).toString());
                txtCedula.setText(tbClientes.getValueAt(row, 1).toString());
                txtTelefono.setText(tbClientes.getValueAt(row, 2).toString());

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error al cargar el cliente");
            }
        }
    }//GEN-LAST:event_tbClientesMouseClicked

    private void btnEliminarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarClienteActionPerformed
        eliminarCliente();
    }//GEN-LAST:event_btnEliminarClienteActionPerformed

    private void btnEditarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarClienteActionPerformed
        int filaselecionada = tbClientes.getSelectedRow();
        if (filaselecionada >= 0) {
            modificar();
            limpiarParametros();         
            limpiarTabla();
                   
            JOptionPane.showMessageDialog(this, "Cliente editado de manera exitosa");
             cargarTabla();
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un cliente para poder editarlo");
        }

    }//GEN-LAST:event_btnEditarClienteActionPerformed

    private void btnRegistroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistroActionPerformed
          try{
            int n = JOptionPane.showConfirmDialog(null, "Desea agregar un nuevo Cliente", "Mensaje de confirmacion",
                    JOptionPane.YES_NO_OPTION);

            if (n == 0) {
                Cliente cli = new Cliente();
                cli.setNombre(txtNombre.getText().trim());
                cli.setIdentificacion(txtCedula.getText());
                cli.setTelefono(txtTelefono.getText().trim());
                log.procesarCliente(cli);
                 limpiarTabla();
                limpiarParametros();                           
                System.out.println("Cliente registrado correctamente");
                 cargarTabla();
            }
        } catch (Exception e) {

        }     
    }//GEN-LAST:event_btnRegistroActionPerformed

    private void btnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegresarActionPerformed
        dispose();
        FrmPrincipal frmp = new FrmPrincipal(login);
        frmp.setVisible(true);
    }//GEN-LAST:event_btnRegresarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmClientes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEditarCliente;
    private javax.swing.JButton btnEliminarCliente;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JButton btnRegresar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbClientes;
    private javax.swing.JTextField txtCedula;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtTelefono;
    // End of variables declaration//GEN-END:variables

    public void cargarTabla() {
        log.cargarClientes();
        LinkedList<Cliente> clientes = log.getClientes();
        tbClientes.setModel(model);        
        for (Cliente temp : clientes) {
            Object[] row = {temp.getNombre(), temp.getCedula(), temp.getTelefono()};
            model.addRow(row);
        }
    }

    public void limpiarTabla() {
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    public void eliminarCliente() {
        int filaselecionada = tbClientes.getSelectedRow();
        LinkedList temp = log.getClientes();
        if (filaselecionada >= 0) {
            log.eliminarCliente(tbClientes.getValueAt(filaselecionada, 1).toString());
            model.removeRow(filaselecionada);
            limpiarParametros();
            JOptionPane.showMessageDialog(this, "Cliente borrado de manera exitosa");
            limpiarTabla();
            cargarTabla();
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un cliente para poder borrarlo");
        }

    }

    public void limpiarParametros() {
        txtNombre.setText("");
        txtCedula.setText("");
        txtTelefono.setText("");
    }

    public void modificar(){
        Cliente cli = new Cliente();
        cli.setNombre(txtNombre.getText().trim());
        cli.setIdentificacion(txtCedula.getText());
        cli.setTelefono(txtTelefono.getText().trim());
        log.modificarCliente(txtCedula.getText(), cli);
    }
}

