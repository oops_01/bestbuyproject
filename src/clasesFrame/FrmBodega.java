package clasesFrame;

import CatalogosCRUD.*;
import bestbuy.*;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import login.Usuario;

/**
 *
 * @author JUAN-PC
 */
public final class FrmBodega extends javax.swing.JFrame {

    LogicaBodega logicaBod;
    LogicaArticulo logicaArt;
    private final DefaultTableModel model;
    private final DefaultTableModel modelo;

    private Usuario login;
    /**
     * Creates new form FrmArticulos
     */
    public FrmBodega() {
        initComponents();
        setLocationRelativeTo(null);
        login = new Usuario();
        logicaBod = new LogicaBodega();
        logicaArt = new LogicaArticulo();
        model = (DefaultTableModel) tbBodega.getModel();
        modelo = (DefaultTableModel) tbArticulos.getModel();
        cargarTablaBodegas();
        cargarTablaArticulos();
    }
     public FrmBodega(Usuario login) {
        initComponents();
        setLocationRelativeTo(null);
         this.login = login;
        logicaBod = new LogicaBodega();
        logicaArt = new LogicaArticulo();
        model = (DefaultTableModel) tbBodega.getModel();
        modelo = (DefaultTableModel) tbArticulos.getModel();
        cargarTablaBodegas();
        cargarTablaArticulos();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtNombreArticulo = new javax.swing.JTextField();
        txtCodigoArticulo = new javax.swing.JTextField();
        txtDescripcionArticulo = new javax.swing.JTextField();
        txtPrecioArticulo = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbBodega = new javax.swing.JTable();
        btnAlmacenarArticulo = new javax.swing.JButton();
        btnRealizarPedido = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnCrearBodega = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtNombreBode = new javax.swing.JTextField();
        txtDescriBode = new javax.swing.JTextField();
        txtCodigoBode = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbArticulos = new javax.swing.JTable();
        btnEliminar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        lblArticulo = new javax.swing.JLabel();
        btnRegresar = new javax.swing.JButton();
        btnEditarBodega = new javax.swing.JButton();
        btnEliminarBode = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 255, 0));
        jLabel1.setText("Almacenar articulo");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(95, 295, -1, -1));

        jLabel2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 153));
        jLabel2.setText("Nombre");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(31, 349, -1, -1));

        jLabel3.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 153));
        jLabel3.setText("Codigo");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 380, -1, -1));

        jLabel4.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 153));
        jLabel4.setText("Descripcion");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(23, 413, -1, -1));

        jLabel5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 153));
        jLabel5.setText("precio");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 440, -1, -1));
        getContentPane().add(txtNombreArticulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 350, 158, -1));

        txtCodigoArticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodigoArticuloActionPerformed(evt);
            }
        });
        getContentPane().add(txtCodigoArticulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 380, 158, -1));

        txtDescripcionArticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescripcionArticuloActionPerformed(evt);
            }
        });
        getContentPane().add(txtDescripcionArticulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 410, 158, -1));
        getContentPane().add(txtPrecioArticulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 440, 158, -1));

        tbBodega.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre", "Codigo", "Descripcion"
            }
        ));
        tbBodega.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbBodegaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbBodega);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(452, 50, 361, 137));

        btnAlmacenarArticulo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnAlmacenarArticulo.setText("Almacenar Articulo");
        btnAlmacenarArticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlmacenarArticuloActionPerformed(evt);
            }
        });
        getContentPane().add(btnAlmacenarArticulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 480, -1, -1));

        btnRealizarPedido.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnRealizarPedido.setText("Realizar Pedido");
        btnRealizarPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRealizarPedidoActionPerformed(evt);
            }
        });
        getContentPane().add(btnRealizarPedido, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 480, -1, -1));

        jLabel6.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 255, 0));
        jLabel6.setText("Bodegas");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 20, -1, -1));

        jLabel7.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(51, 255, 0));
        jLabel7.setText("Crear Bodega");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(123, 40, -1, -1));

        btnCrearBodega.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnCrearBodega.setText("Crear Bodega");
        btnCrearBodega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearBodegaActionPerformed(evt);
            }
        });
        getContentPane().add(btnCrearBodega, new org.netbeans.lib.awtextra.AbsoluteConstraints(123, 175, -1, -1));

        jLabel8.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Nombre");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 83, -1, -1));

        jLabel9.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Codigo");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(44, 109, -1, -1));

        jLabel10.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Descripcion");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(31, 140, -1, -1));
        getContentPane().add(txtNombreBode, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 80, 160, -1));
        getContentPane().add(txtDescriBode, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 137, 160, -1));
        getContentPane().add(txtCodigoBode, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, 160, -1));

        tbArticulos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Bodega", "Codigo Bodega", "Articulo", "Codigo Articulo", "Precio", "Descripcion", "Existencias"
            }
        ));
        tbArticulos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbArticulosMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbArticulos);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(452, 331, 610, 137));

        btnEliminar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnEliminar.setText("Eliminar Articulo");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        getContentPane().add(btnEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 480, -1, -1));

        btnEditar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnEditar.setText("Trasladar Articulo");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        getContentPane().add(btnEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 480, -1, -1));
        getContentPane().add(lblArticulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 524, 89, 23));

        btnRegresar.setText("<< Regresar");
        btnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegresarActionPerformed(evt);
            }
        });
        getContentPane().add(btnRegresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 9, 103, -1));

        btnEditarBodega.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnEditarBodega.setText("Editar Bodega");
        btnEditarBodega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarBodegaActionPerformed(evt);
            }
        });
        getContentPane().add(btnEditarBodega, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 200, -1, -1));

        btnEliminarBode.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnEliminarBode.setText("Eliminar Bodega");
        btnEliminarBode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarBodeActionPerformed(evt);
            }
        });
        getContentPane().add(btnEliminarBode, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 200, -1, -1));

        jLabel11.setForeground(new java.awt.Color(51, 255, 0));
        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bodegas.jpg"))); // NOI18N
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 560));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCrearBodegaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearBodegaActionPerformed
        try {
            int n = JOptionPane.showConfirmDialog(null, "Desea agregar un nueva bodega", "Mensaje de confirmacion",
                    JOptionPane.YES_NO_OPTION);

            if (n == 0) {
                Bodega bodega = new Bodega();
                bodega.setNombre(txtNombreBode.getText().trim());
                bodega.setCodigo(txtCodigoBode.getText());
                bodega.setDescripcion(txtDescriBode.getText().trim());
                logicaBod.procesarBodega(bodega);
                limpiarTablaBodega();
                limpiarParametrosBodega();
                System.out.println("Bodega registrada correctamente");
                cargarTablaBodegas();
            }
        } catch (Exception e) {

        }
    }//GEN-LAST:event_btnCrearBodegaActionPerformed

    private void btnAlmacenarArticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlmacenarArticuloActionPerformed
        int n = JOptionPane.showConfirmDialog(null, "Desea agregar un nueva bodega", "Mensaje de confirmacion",
      JOptionPane.YES_NO_OPTION);
        int existencias=0;
        if (n == 0) {
            int row = tbBodega.getSelectedRow();
            if (row >= 0) {
                String nombreBodega = tbBodega.getValueAt(row, 0).toString();
                String codigoBodega = tbBodega.getValueAt(row, 1).toString();
                Articulo arti = new Articulo();
                arti.setNombre(txtNombreArticulo.getText());
                arti.setCodigo(txtCodigoArticulo.getText());
                arti.setDescripcion(txtDescripcionArticulo.getText());
                arti.setPrecio(Double.parseDouble(txtPrecioArticulo.getText()));
                arti.setBodegaOrigen(nombreBodega);
                arti.setCodigoBodega(codigoBodega);
                arti.setExistencias(existencias);
                logicaArt.procesarArticulo(arti);
                limpiarTablaArticulo();

                cargarTablaArticulos();
                System.out.println("Articulo registrado correctamente");
//                txtCodigoArticulo.setText("");
//                txtPrecioArticulo.setText("");
//                txtDescripcionArticulo.setText("");
//                txtNombreArticulo.setText("");
               limpiarParametrosArticulos();
               limpiarParametrosBodega();
            } else {
                JOptionPane.showMessageDialog(null, "Para Agregar un Articulo debe seleccionar una bodega");
            }

        }

    }//GEN-LAST:event_btnAlmacenarArticuloActionPerformed

    private void txtCodigoArticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodigoArticuloActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoArticuloActionPerformed

    private void txtDescripcionArticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripcionArticuloActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripcionArticuloActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        eliminarArticulos();
        limpiarTablaArticulo();
        cargarTablaArticulos();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegresarActionPerformed
        dispose();
        FrmPrincipal frmp = new FrmPrincipal(login);
        frmp.setVisible(true);

    }//GEN-LAST:event_btnRegresarActionPerformed

    private void btnRealizarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRealizarPedidoActionPerformed
        int elemento = tbArticulos.getSelectedRow();
        if (elemento >= 0) {
            String nombreArticulo = tbArticulos.getValueAt(elemento, 2).toString();
            String codigoArticulo = tbArticulos.getValueAt(elemento, 3).toString();
            FrmDistribuidoras frmd = new FrmDistribuidoras(login,nombreArticulo, codigoArticulo);
            frmd.setVisible(true);
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar el articulo que desea aumentar");
        }
    }//GEN-LAST:event_btnRealizarPedidoActionPerformed

    private void tbArticulosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbArticulosMouseClicked
    int row = tbArticulos.getSelectedRow();
        if (row >= 0) {
            try {
                txtNombreArticulo.setText(tbArticulos.getValueAt(row, 2).toString());
                txtCodigoArticulo.setText(tbArticulos.getValueAt(row, 3).toString());
                txtPrecioArticulo.setText(tbArticulos.getValueAt(row, 4).toString());
                txtDescripcionArticulo.setText(tbArticulos.getValueAt(row, 5).toString());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error al cargar el articulo");
            }
        }
    }//GEN-LAST:event_tbArticulosMouseClicked

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
//        try {
//            
//        } catch (Exception e) {
//        }
        int filaselecionada = tbArticulos.getSelectedRow();
       int row =tbBodega.getSelectedRow();
        if (filaselecionada >= 0 && row>=0) {
            String nombreBodega=tbBodega.getValueAt(row, 0).toString();
            String codigoBodega=tbBodega.getValueAt(row, 1).toString();
            modificarArticulos(nombreBodega,codigoBodega);
            limpiarParametrosArticulos(); 
            limpiarTablaArticulo();                  
            JOptionPane.showMessageDialog(this, "Articulo trasladado de manera exitosa");
             cargarTablaArticulos();
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un articulo y una bodega para poder trasladarlo");
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnEditarBodegaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarBodegaActionPerformed
             int filaselecionada = tbBodega.getSelectedRow();
        if (filaselecionada >= 0) {
            modificarBodega();
            limpiarParametrosBodega();         
            limpiarTablaBodega();
                   
            JOptionPane.showMessageDialog(this, "Cliente editado de manera exitosa");
             cargarTablaBodegas();
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un cliente para poder editarlo");
        }
    }//GEN-LAST:event_btnEditarBodegaActionPerformed

    private void tbBodegaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbBodegaMouseClicked
           int row = tbBodega.getSelectedRow();
        if (row >= 0) {
            try {
                txtNombreBode.setText(tbBodega.getValueAt(row, 0).toString());
                txtCodigoBode.setText(tbBodega.getValueAt(row, 1).toString());
                txtDescriBode.setText(tbBodega.getValueAt(row, 2).toString());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error al cargar la bodega");
            }
        }
    }//GEN-LAST:event_tbBodegaMouseClicked

    private void btnEliminarBodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarBodeActionPerformed
        eliminarBodega();
    }//GEN-LAST:event_btnEliminarBodeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmBodega.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmBodega.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmBodega.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmBodega.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmBodega().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlmacenarArticulo;
    private javax.swing.JButton btnCrearBodega;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEditarBodega;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnEliminarBode;
    private javax.swing.JButton btnRealizarPedido;
    private javax.swing.JButton btnRegresar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblArticulo;
    private javax.swing.JTable tbArticulos;
    private javax.swing.JTable tbBodega;
    private javax.swing.JTextField txtCodigoArticulo;
    private javax.swing.JTextField txtCodigoBode;
    private javax.swing.JTextField txtDescriBode;
    private javax.swing.JTextField txtDescripcionArticulo;
    private javax.swing.JTextField txtNombreArticulo;
    private javax.swing.JTextField txtNombreBode;
    private javax.swing.JTextField txtPrecioArticulo;
    // End of variables declaration//GEN-END:variables

    public void cargarTablaBodegas() {
        try {
            logicaBod.cargarBodegas();
        LinkedList<Bodega> bodegas = logicaBod.getBodegas();
        tbBodega.setModel(model);
        for (Bodega temp : bodegas) {
            Object[] row = {temp.getNombre(), temp.getCodigo(), temp.getDescripcion()};
            model.addRow(row);
        }   
        } catch (Exception e) {
        }
     
    }

    public void limpiarTablaBodega() {
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    public void limpiarTablaArticulo() {
        for (int i = modelo.getRowCount() - 1; i >= 0; i--) {
            modelo.removeRow(i);
        }
    }

    public void eliminarBodega() {
        int filaselecionada = tbBodega.getSelectedRow();
        LinkedList temp = logicaBod.getBodegas();
        if (filaselecionada >= 0) {
            logicaBod.eliminarBodega(tbBodega.getValueAt(filaselecionada, 1).toString());
            model.removeRow(filaselecionada);
            limpiarParametrosBodega();
            JOptionPane.showMessageDialog(this, "Bodega borrada de manera exitosa");
            limpiarTablaBodega();
            cargarTablaBodegas();
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar una bodega para poder borrarlo");
        }

    }

    public void limpiarParametrosBodega() {
        txtNombreBode.setText("");
        txtCodigoBode.setText("");
        txtDescriBode.setText("");
    }

    public void modificarBodega() {
        Bodega bodega = new Bodega();
        bodega.setNombre(txtNombreBode.getText().trim());
        bodega.setCodigo(txtCodigoBode.getText());
        bodega.setDescripcion(txtDescriBode.getText().trim());
        logicaBod.modificarBodega(txtCodigoBode.getText(), bodega);
    }

    public void cargarTablaArticulos() {
        logicaArt.cargarArticulos();
        LinkedList<Articulo> articulos = logicaArt.getArticulos();
        tbArticulos.setModel(modelo);
        for (Articulo temp : articulos) {
            Object[] row = {temp.getBodegaOrigen(), temp.getCodigoBodega(),
                temp.getNombre(), temp.getCodigo(), temp.getPrecio(), temp.getDescripcion(),temp.getExistencias()
            };
            modelo.addRow(row);
        }
    }

    public void eliminarArticulos() {
        int row = tbArticulos.getSelectedRow();
        LinkedList temp = logicaArt.getArticulos();
        if (row >= 0) {
            logicaArt.eliminarArticulo(tbArticulos.getValueAt(row, 1).toString());
            modelo.removeRow(row);
            limpiarParametrosArticulos();
            JOptionPane.showMessageDialog(this, "Articulo borrado de manera exitosa");
            limpiarTablaBodega();
            cargarTablaArticulos();
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un  para poder borrarlo");
        }

    }

    public void limpiarParametrosArticulos() {
        txtNombreArticulo.setText("");
        txtCodigoArticulo.setText("");
        txtDescripcionArticulo.setText("");
        txtPrecioArticulo.setText("");
    }

    public void modificarArticulos(String nombrebodega,String codigoBodega) {
        Articulo articulo = new Articulo();
        articulo.setNombre(txtNombreArticulo.getText().trim());
        articulo.setCodigo(txtCodigoArticulo.getText().trim());
        articulo.setDescripcion(txtDescripcionArticulo.getText().trim());
        articulo.setPrecio(Double.parseDouble(txtPrecioArticulo.getText().trim()));
        articulo.setBodegaOrigen(nombrebodega);
        articulo.setCodigoBodega(codigoBodega);
        logicaArt.modificarArticulo(txtCodigoBode.getText(), articulo);
    }
       
}
