package clasesFrame;

import CatalogosCRUD.*;
import bestbuy.Agente;
import bestbuy.Articulo;
import bestbuy.Distribuidora;
import java.awt.HeadlessException;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import login.Usuario;

/**
 *
 * @author JUAN-PC
 */
public final class FrmDistribuidoras extends javax.swing.JFrame {
       LogicaAgentes logAge;
     private final DefaultTableModel model;
     private final DefaultTableModel modelo;
     LogicaDistribuidora distribuidora;
     LogicaAgentes agentes;     
     private Usuario login;
     private String codigoArticulo;
     LogicaArticulo logAr;
     Articulo ar;
    public FrmDistribuidoras() {
        initComponents();
        setLocationRelativeTo(null);
        login = new Usuario();
        ar = new Articulo();
        logAge = new LogicaAgentes();
         model = (DefaultTableModel) tbDistri.getModel();
         modelo = (DefaultTableModel) tbAgentes.getModel();
         distribuidora = new LogicaDistribuidora();
         agentes = new LogicaAgentes();
         logAr = new LogicaArticulo();
         cargarTablaDistri();     
          distribuidoraAleatoria();
             cargarTablaAgentes();
    }
        public FrmDistribuidoras(Usuario login , String nombreArticulo,String codArticulo) {
        initComponents();
        setLocationRelativeTo(null);
        ar = new Articulo();
         this.login = login;
        logAge = new LogicaAgentes();
        modelo = (DefaultTableModel) tbAgentes.getModel();
        model = (DefaultTableModel) tbDistri.getModel();
        distribuidora = new LogicaDistribuidora();
        agentes = new LogicaAgentes(); 
        logAr = new LogicaArticulo();
        distribuidoraAleatoria();
        cargarTablaAgentes();
        cargarTablaDistri();
        lblArticulo.setText(nombreArticulo);
        this.codigoArticulo = codArticulo;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnRegresar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtIdentificacion = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbDistri = new javax.swing.JTable();
        btnRegistrar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblDistribuidora = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbAgentes = new javax.swing.JTable();
        lblAsignarDistri = new javax.swing.JLabel();
        btnAumentarCantidad = new javax.swing.JButton();
        lblArticulo = new javax.swing.JLabel();
        spinerAumentarCantidad = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnRegresar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnRegresar.setText("<< Regresar");
        btnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegresarActionPerformed(evt);
            }
        });
        getContentPane().add(btnRegresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 103, -1));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Nombre");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, -1, -1));

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Código de Distribuidora");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, -1, -1));
        getContentPane().add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 110, 185, -1));

        txtIdentificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdentificacionActionPerformed(evt);
            }
        });
        getContentPane().add(txtIdentificacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 160, 185, -1));

        tbDistri.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre", "Número de Cédula"
            }
        ));
        tbDistri.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbDistriMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbDistri);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 70, 310, 121));

        btnRegistrar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnRegistrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 210, -1, -1));

        btnEliminar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        getContentPane().add(btnEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 210, -1, -1));

        btnEditar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        getContentPane().add(btnEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 210, -1, -1));

        jLabel3.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Distribuidoras");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(113, 46, -1, -1));

        jLabel4.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Codigo de Distribuidora");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 260, -1, -1));

        lblDistribuidora.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(lblDistribuidora, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 290, 260, 30));

        tbAgentes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "nombre", "Cedula", "Telefono"
            }
        ));
        jScrollPane2.setViewportView(tbAgentes);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 330, -1, 140));

        lblAsignarDistri.setForeground(new java.awt.Color(0, 0, 204));
        getContentPane().add(lblAsignarDistri, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 260, 130, 20));

        btnAumentarCantidad.setText("Aumentar Existencias");
        btnAumentarCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAumentarCantidadActionPerformed(evt);
            }
        });
        getContentPane().add(btnAumentarCantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 340, -1, 30));

        lblArticulo.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(lblArticulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 310, 130, 20));

        spinerAumentarCantidad.setModel(new javax.swing.SpinnerNumberModel(1, 1, 1000, 1));
        getContentPane().add(spinerAumentarCantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 340, 73, 30));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/distibuidoras.jpg"))); // NOI18N
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 820, 540));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtIdentificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdentificacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdentificacionActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        int filaselecionada = tbDistri.getSelectedRow();
        if (filaselecionada >= 0) {
            modificar();
            limpiarParametros();
            limpiarTabla();

            JOptionPane.showMessageDialog(this, "Cliente editado de manera exitosa");
            cargarTablaDistri();
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un cliente para poder editarlo");
        }

    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
      try {
            int n = JOptionPane.showConfirmDialog(null, "Desea agregar una nueva distribuidora", "Mensaje de confirmacion",
                    JOptionPane.YES_NO_OPTION);

            if (n == 0) {
                Distribuidora dis = new Distribuidora();
                dis.setNombre(txtNombre.getText().trim());
                dis.setIdentificacion(txtIdentificacion.getText());
                distribuidora.crearDistribuidora(dis);
                limpiarParametros();
                limpiarTabla();
                System.out.println("Cliente registrado correctamente");
                cargarTablaDistri();


            }
        } catch (HeadlessException e) {
            System.out.println("ops");
        }
    
    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void tbDistriMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbDistriMouseClicked
        int row = tbDistri.getSelectedRow();
        if (row >= 0) {
            try {
                txtNombre.setText(tbDistri.getValueAt(row, 0).toString());
                txtIdentificacion.setText(tbDistri.getValueAt(row, 1).toString());              
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Error al cargar agentes");
            }
        }


    }//GEN-LAST:event_tbDistriMouseClicked

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        eliminarAgente();    
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegresarActionPerformed
        dispose();
        FrmPrincipal frmp = new FrmPrincipal(login);
        frmp.setVisible(true);
    }//GEN-LAST:event_btnRegresarActionPerformed

    private void btnAumentarCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAumentarCantidadActionPerformed
        try {
        String codigo1 = codigoArticulo;
          LogicaFactura logfac = new LogicaFactura();
          ar = logfac.buscarArticulo(codigo1);
           int existencias =  ar.getExistencias()+((int) spinerAumentarCantidad.getValue());
          System.out.println(existencias);
        Articulo  a = new Articulo();
         a.setNombre(ar.getNombre());
         a.setCodigo(ar.getCodigo());
         a.setDescripcion(ar.getDescripcion());
         a.setPrecio(ar.getPrecio());
         a.setBodegaOrigen(ar.getBodegaOrigen());
         a.setCodigoBodega(ar.getCodigoBodega());
         a.setExistencias(existencias);
        logAr.modificarArticulo(codigo1,a); 
        } catch (Exception e) {
        }
      
    }//GEN-LAST:event_btnAumentarCantidadActionPerformed
    public void cargarTablaDistri() {
        
 
       limpiarTabla();
        distribuidora.cargarDistribuidoras();
        LinkedList<Distribuidora> distribuidoras = distribuidora.getDistribuidoras();
        tbDistri.setModel(model);
        for (int i = 0; i <distribuidoras.size(); i++) {
            Object[] row = { distribuidoras.get(i).getNombre(),distribuidoras.get(i).getIdentificacion()};
        model.addRow(row);
        }   
    }

     public void cargarTablaAgentes() {
        limpiarTabla();
         LinkedList<Agente> agent = agentes.getAgentes();          
        if (!lblAsignarDistri.getText().equals("")) {
            for (int i = 0; i < agent.size(); i++) {
                if (agent.get(i).getCodigoDistribuidora().equals(lblAsignarDistri.getText())) {
                 lblDistribuidora.setText("Agentes de la Distribuidora : " + agent.get(i).getNombre());   
               Object[] row = {agent.get(i).getNombre(),agent.get(i).getIdentificacion(),agent.get(i).getTelefono()};
                modelo.addRow(row);                   
                }else{
                    
                }
 
            }
        } 
     }

    public void limpiarTabla() {
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    public void eliminarAgente() {
        int filaselecionada = tbDistri.getSelectedRow();
        LinkedList temp = distribuidora.getDistribuidoras();
        
        if (filaselecionada >= 0) {
            distribuidora.eliminarDistribuidora(tbDistri.getValueAt(filaselecionada, 1).toString());
            model.removeRow(filaselecionada);
            limpiarParametros();
            JOptionPane.showMessageDialog(this, "Distribuidora Eliminada de manera exitosa");
            limpiarTabla();
            cargarTablaDistri();
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un agente para poder borrarlo");
        }

    }

    public void limpiarParametros() {
        txtNombre.setText("");
        txtIdentificacion.setText("");

    }

    public void modificar() {
        Distribuidora dis = new Distribuidora();
        dis.setNombre(txtNombre.getText().trim());
        dis.setIdentificacion(txtIdentificacion.getText());
        distribuidora.modificarDistribuidoras(txtIdentificacion.getText(), dis);
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmDistribuidoras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmDistribuidoras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmDistribuidoras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmDistribuidoras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmDistribuidoras().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAumentarCantidad;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JButton btnRegresar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblArticulo;
    private javax.swing.JLabel lblAsignarDistri;
    private javax.swing.JLabel lblDistribuidora;
    private javax.swing.JSpinner spinerAumentarCantidad;
    private javax.swing.JTable tbAgentes;
    private javax.swing.JTable tbDistri;
    private javax.swing.JTextField txtIdentificacion;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
public void distribuidoraAleatoria(){
   
 Distribuidora  dis= distribuidora.asignarDistribuidora();
      lblAsignarDistri.setText(dis.getIdentificacion());
      lblDistribuidora.setText("Distribuidora: " + dis.getNombre());
}
}
