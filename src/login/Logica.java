/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import java.util.LinkedList;

/**
 *
 * @author JUAN-PC
 */
public final class Logica {
    private LinkedList<Usuario> usuarios;

    public Logica(LinkedList<Usuario> usuarios) {
        this.usuarios = usuarios;
         usuarios = new LinkedList<>();
        cargarUsuarios();       
    }
   
    public Logica() {
        usuarios = new LinkedList<>();
        cargarUsuarios();
    }
    public Usuario login(Usuario u){
        for (int i = 0; i <usuarios.size(); i++) {
           Usuario temp = usuarios.get(i);
           if(temp.getLogin().equals(u.getLogin())
                   && temp.getPassword().equals(u.getPassword())){
               System.out.println(temp.getRoles());
               return temp;
               
           }
        }
    return null;
    }

    public void procesarUsuario(Usuario u){
        ManejadorArchivos archivo = new ManejadorArchivos();
    String texto = archivo.leerTextoArchivo("usuarios.txt");
        archivo.escribirTextoArchivo("usuarios.txt", texto
                + u.getLogin()
                + ";" + u.getPassword()
                + ";" + u.getRoles());
    }
    
     public void cargarUsuarios() {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String datos = archivo.leerTextoArchivo("usuarios.txt");
        String[] aux = datos.split("\n");
        if (!datos.trim().equals("")) {
            usuarios = new LinkedList<>();
            for (String aux1 : aux) {
                String[] cl = aux1.split(";");
                Usuario u = new Usuario();
                u.setLogin(cl[0]);
                u.setPassword(cl[1]);
                u.setRoles(cl[2]);               
                usuarios.add(u);
            }

        } else {
            usuarios = new LinkedList<>();

        }
    }
        public void eliminarUsuario(String username) {
        String listaUsuarios = "";
        for (int i = 0; i < usuarios.size(); i++) {
            if (!usuarios.get(i).getUsername().equals(username)) {
                listaUsuarios += usuarios.get(i).getUsername()+ ";"
                        + usuarios.get(i).getPassword()+ ";"
                        + usuarios.get(i).getRoles()+ "\n";
            }
        }
        ManejadorArchivos archivo = new ManejadorArchivos();
        archivo.escribirTextoArchivo("usuarios.txt", listaUsuarios);
        cargarUsuarios();
    }
        
     
     
     

    public LinkedList<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(LinkedList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
     public void modificarUsuario(Usuario user ,String identificacion) {
        eliminarUsuario(identificacion);
        procesarUsuario(user);
    }
     
     
}
