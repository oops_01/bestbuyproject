/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author JUAN-PC
 */
public class Usuario {
    private String username;
    private  String password;
    private String roles;
    private LinkedList <Usuario> usuarios;
    
    public Usuario() {
    }

    public Usuario(String username, String password, String roles, LinkedList<Usuario> usuarios) {
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.usuarios = usuarios;
    }

    public String getLogin() {
        return username;
    }

    public void setLogin(String login) {
        this.username = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Usuario{" + "username=" + username + ", password=" + password + ", roles=" + roles + ", usuarios=" + usuarios + '}';
    }
    
    

  
    public LinkedList<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(LinkedList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

       @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        return true;
    }
    
    
    
}
