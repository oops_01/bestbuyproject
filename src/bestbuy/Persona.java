/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestbuy;

/**
 *Esta es una super clase persona 
 * @author evarg
 */
public class Persona {
    
    String identificacion;
    String nombre;
    String telefono;

    public Persona() {
    }

    public Persona(String cedula, String nombre, String telefono) {
        this.identificacion = cedula;
        this.nombre = nombre;
        this.telefono = telefono;
    }

    public String getCedula() {
        return identificacion;
    }

    public void setCedula(String cedula) {
        this.identificacion = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return identificacion + nombre + telefono ;
    }

 
}
