/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestbuy;

import java.util.ArrayList;


/**
 *esta clase es una entidad de un articulo
 * @author JUAN-PC
 */
public class Articulo {

    private int cantidad;
    
    
    private String nombre;
    private String codigo;
    private String descripcion;
    private double precio;
    private String bodegaOrigen;
    private String codigoBodega;
    private int existencias;

    public Articulo() {
    }

    public Articulo(String nombre, String codigo, String descripcion, 
            double precio, String bodegaOrigen, String codigoBodega,
            int existencias, ArrayList<Distribuidora> proveedores,int cantidad) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio;
        this.bodegaOrigen = bodegaOrigen;
        this.codigoBodega = codigoBodega;
        this.existencias = existencias;
        this.proveedores = proveedores;
         this.cantidad = cantidad;
    }

    private ArrayList<Distribuidora> proveedores;

 

    public int getExistencias() {
        return existencias;
    }

    public void setExistencias(int existencias) {
        this.existencias = existencias;
    }

    public ArrayList<Distribuidora> getProveedores() {
        return proveedores;
    }

    public void setProveedores(ArrayList<Distribuidora> proveedores) {
        this.proveedores = proveedores;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getBodegaOrigen() {
        return bodegaOrigen;
    }

    public void setBodegaOrigen(String bodegaOrigen) {
        this.bodegaOrigen = bodegaOrigen;
    }

    public String getCodigoBodega() {
        return codigoBodega;
    }

    public void setCodigoBodega(String codigoBodega) {
        this.codigoBodega = codigoBodega;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "Articulo{" + "cantidad=" + cantidad + ", nombre=" + nombre + ", codigo=" + codigo + ", descripcion=" + descripcion + ", precio=" + precio + ", bodegaOrigen=" + bodegaOrigen + ", codigoBodega=" + codigoBodega + ", existencias=" + existencias + ", proveedores=" + proveedores + '}';
    }
    
  

}
