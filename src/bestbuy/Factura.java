/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestbuy;

import java.util.ArrayList;

/**
 *clase esta es una entidad de factura 
 * @author JUAN-PC
 */
public class Factura extends DetalleFactura {

    Cliente cliente;
    MedioPago medioPago;
    ArrayList<DetalleFactura> detalles;

    
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

}
