/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestbuy;


/**
 *esta clase es una entidad de tipo agente
 * @author evarg
 */
public class Agente extends Persona {

    private String codigoDistribuidora;
    private String nombreDistribuidora;
    public void procesarPedido(Articulo articulo, int cantidad) {
     
    }

    public Agente() {
    }
    

    public Agente(String codigoBodega, String nombreBodega) {
        this.codigoDistribuidora = codigoBodega;
        this.nombreDistribuidora = nombreBodega;
    }
    

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigoDistribuidora() {
        return codigoDistribuidora;
    }

    public void setCodigoDistribuidora(String codigoDistribuidora) {
        this.codigoDistribuidora = codigoDistribuidora;
    }

    public String getNombreDistribuidora() {
        return nombreDistribuidora;
    }

    public void setNombreDistribuidora(String nombreDistribuidora) {
        this.nombreDistribuidora = nombreDistribuidora;
    }


}
