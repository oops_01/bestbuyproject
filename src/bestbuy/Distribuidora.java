/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestbuy;

/**
 *esta es una entidad de distribuidora
 * @author JUAN-PC
 */
public class Distribuidora {

    private String identificacion;
    private String nombre;

    public Distribuidora() {
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Distribuidora(String identificacion, String nombre) {
        this.identificacion = identificacion;
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Distribuidora{" + "identificacion=" + identificacion + ", nombre=" + nombre + '}';
    }

  
}
