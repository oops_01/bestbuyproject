
package bestbuy;

/**
 * Esta clase es una entidad de tipo detalle factura
 * @author evarg
 */
public class DetalleFactura {
    
   private Articulo articulo;
   private  int cantidad;
   private double subTotal;
   private double impuestos;

    public DetalleFactura() {
    }

    public DetalleFactura(Articulo articulo, int cantidad, double subTotal, double impuestos) {
        this.articulo = articulo;
        this.cantidad = cantidad;
        this.subTotal = subTotal;
        this.impuestos = impuestos;
    }
   
   
   

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(double impuestos) {
        this.impuestos = impuestos;
    }

    @Override
    public String toString() {
        return  " " + articulo +""+ cantidad +""+ subTotal  +""+ impuestos ;
    }
   
   
   
    
    
}
