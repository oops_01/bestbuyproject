/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CatalogosCRUD;

import bestbuy.*;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import login.ManejadorArchivos;

/**
 *
 * @author JUAN-PC
 */
public final class LogicaBodega {
        Timer timer;
    private LinkedList<Bodega> bodegas;

    public LogicaBodega(LinkedList<Bodega> bodegas) {
        this.bodegas = bodegas;
    }

    public LogicaBodega() {
        bodegas = new LinkedList<>();
        cargarBodegas();
        refrescar();
    }
/**
 * agrega las bodegas a un txt
 * @param bodegas 
 */
    public void procesarBodega(Bodega bodegas) {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String texto = archivo.leerTextoArchivo("bodegas.txt");
        archivo.escribirTextoArchivo("bodegas.txt", texto
                + bodegas.getNombre()
                + ";" + bodegas.getCodigo()
                + ";" + bodegas.getDescripcion());
    }
/**
 * carga las bodegas del txt
 */
    public void cargarBodegas() {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String datos = archivo.leerTextoArchivo("bodegas.txt");
        String[] aux = datos.split("\n");
        if (!datos.trim().equals("")) {
            bodegas = new LinkedList<>();
            for (String aux1 : aux) {
                String[] cl = aux1.split(";");
                Bodega c = new Bodega();
                c.setNombre(cl[0]);
                c.setCodigo(cl[1]);
                c.setDescripcion(cl[2]);

                bodegas.add(c);
            }

        } else {

            bodegas = new LinkedList<>();

        }
    }
/**
 * elimina las bodegas del txt
 * @param identificacion 
 */
    public void eliminarBodega(String identificacion) {
        String listaBodegas = "";
        for (int i = 0; i < bodegas.size(); i++) {
            if (!bodegas.get(i).getCodigo().equals(identificacion)) {
                listaBodegas += bodegas.get(i).getNombre() + ";"
                        + bodegas.get(i).getCodigo()+ ";"
                        + bodegas.get(i).getDescripcion()+ "\n";
            }
        }
        ManejadorArchivos archivo = new ManejadorArchivos();
        archivo.escribirTextoArchivo("bodegas.txt", listaBodegas);
        cargarBodegas();
    }
/**
 * modifica una bodega en el txt
 * @param identificacion
 * @param bodegas 
 */
    public void modificarBodega(String identificacion, Bodega bodegas) {
        eliminarBodega(identificacion);
        procesarBodega(bodegas);
    }

    public LinkedList<Bodega> getBodegas() {
        return bodegas;
    }

    public void setBodegas(LinkedList<Bodega> bodegas) {
        this.bodegas = bodegas;
    }
  /**
   * Constantemente el metodo cargar bodega para que se actualicen de inmediato al crear un registro nuevo
   */
    public void refrescar() {
        timer = new Timer();
        TimerTask tarea = new TimerTask() {

            @Override
            public void run() {
                ManejadorArchivos archivo = new ManejadorArchivos();
                String datos = archivo.leerTextoArchivo("bodegas.txt");
                String[] aux = datos.split("\n");
                if (!datos.trim().equals("")) {
                    bodegas = new LinkedList<>();
                    for (int i = 0; i < aux.length; i++) {
                        String[] cl = aux[i].split(";");
                        Bodega c = new Bodega();

                        c.setNombre(cl[0]);
                        c.setCodigo(cl[1]);
                        c.setDescripcion(cl[2]);

                        bodegas.add(c);
                    }

                } else {
                    bodegas = new LinkedList<>();

                }
            }
        };
        timer.schedule(tarea, 0, 1000);
    }
    /**
     * busca una bodega en el txt
     * @param codigo
     * @return 
     */
  public Bodega buscarBodega(String codigo) {
        LinkedList<Bodega> bodegas =getBodegas();
        for (int i = 0; i < getBodegas().size(); i++) {
            Bodega temp = bodegas.get(i);
            if (temp.getCodigo().equals(codigo)) {
                System.out.println(temp.getNombre());
                return temp;

            }
        }
        return null;
    }
}
