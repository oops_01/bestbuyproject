/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CatalogosCRUD;


import bestbuy.*;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import login.ManejadorArchivos;

/**
 *
 * @author JUAN-PC
 */
public final class LogicaDistribuidora {
    Timer timer; 
   private LinkedList<Distribuidora> distribuidoras;

    public LogicaDistribuidora(LinkedList<Distribuidora> distribuidora) {
        this.distribuidoras = distribuidora;
    }
   
    public LogicaDistribuidora() {
    distribuidoras = new LinkedList<>();
    cargarDistribuidoras();
    refrescar();
    }
    
/**
 * agrega las distribuidoras a un txt
 * @param dis 
 */
    public void crearDistribuidora(Distribuidora dis) {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String texto = archivo.leerTextoArchivo("Distribuidora.txt");
        archivo.escribirTextoArchivo("Distribuidora.txt", texto
                + dis.getIdentificacion()
                + ";" + dis.getNombre());
    }
/**
 * elimina las distribuidoras del txt
 * @param identificacion 
 */
    public void eliminarDistribuidora(String identificacion) {
        String listaDistribuidoras = "";   
        for (int i = 0; i < distribuidoras.size(); i++) {
            if ( distribuidoras.get(i).getIdentificacion().equals(identificacion)) {
                listaDistribuidoras += distribuidoras.get(i).getIdentificacion() + ";" +  distribuidoras.get(i).getNombre()  + "\n";
            }
        }
        ManejadorArchivos archivo = new ManejadorArchivos();
        archivo.escribirTextoArchivo("Distribuidora.txt", listaDistribuidoras);
        cargarDistribuidoras();
    }
/**
 * carga las distribuidoras del txt
 */
    public void cargarDistribuidoras() {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String datos = archivo.leerTextoArchivo("Distribuidora.txt");
        String[] aux = datos.split("\n");
        if (!datos.trim().equals("")) {
            distribuidoras = new LinkedList<>();
            for (String aux1 : aux) {
                String[] cl = aux1.split(";"); 
                Distribuidora d = new Distribuidora();
                d.setIdentificacion(cl[0]);
                d.setNombre(cl[1]);             
                distribuidoras.add(d);
            }

        } else {
            distribuidoras = new LinkedList<>();
        }
    }
/**
 * modifica las distribuidoras
 * @param identificacion
 * @param dis 
 */
    public void modificarDistribuidoras(String identificacion, Distribuidora dis) {
        eliminarDistribuidora(identificacion);
        crearDistribuidora(dis);
    }
    /**
     * busca las distribuidoras del txt
     * @param codigo
     * @return 
     */
      public Distribuidora buscarDistribuidora(String codigo) { 
    LinkedList<Distribuidora> distribuidora =getDistribuidoras();
        for (int i = 0; i <getDistribuidoras().size(); i++) {
            Distribuidora temp = distribuidoras.get(i);
            if (temp.getIdentificacion().equals(codigo)) {
                
                return temp;

            }
        }
        return null;
    }

    public LinkedList<Distribuidora> getDistribuidoras() {
        return distribuidoras;
    }

    public void setDistribuidoras(LinkedList<Distribuidora> distribuidoras) {
        this.distribuidoras = distribuidoras;
    }
  
   
     /**
      * Constantemente el metodo cargar distribuidora para que se actualicen de inmediato al crear un registro nuevo
      **/ 
     public void refrescar() {
        timer = new Timer();
        TimerTask tarea = new TimerTask() {

            @Override
            public void run() {
                 ManejadorArchivos archivo = new ManejadorArchivos();
        String datos = archivo.leerTextoArchivo("Distribuidora.txt");
        String[] aux = datos.split("\n");
        if (!datos.trim().equals("")) {
            distribuidoras = new LinkedList<>();
            for (String aux1 : aux) {
                String[] dis = aux1.split(";");
                Distribuidora d = new Distribuidora();
                d.setIdentificacion(dis[0]);
                d.setNombre(dis[1]);
              
                distribuidoras.add(d);
            }

        } else {
            distribuidoras = new LinkedList<>();
        }
            }
        };
        timer.schedule(tarea, 0, 1000);
    }
         /**
          * asigna una distribuidora aleatoria 
          * @return 
          */
       public Distribuidora asignarDistribuidora(){
   int numero = (int) (Math.random() * distribuidoras.size()) + 0;   
           LinkedList<Distribuidora> distribuidoras =getDistribuidoras();
        for (Distribuidora distri : distribuidoras) {
            Distribuidora temp = distribuidoras.get(numero);
            return temp;
    }
        return null;
    }
 

}
