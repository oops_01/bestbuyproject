/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CatalogosCRUD;

import bestbuy.*;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import login.ManejadorArchivos;

/**
 *
 * @author JUAN-PC
 */
public final class LogicaClientes {

    Timer timer;
    private LinkedList<Cliente> clientes;

    public LogicaClientes(LinkedList<Cliente> clientes) {
        this.clientes = clientes;
    }

    public LogicaClientes() {
        clientes = new LinkedList<>();
        cargarClientes();
        refrescar();
    }
/**
 * agrega los clientes a un txt
 * @param cliente 
 */
    public void procesarCliente(Cliente cliente) {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String texto = archivo.leerTextoArchivo("clientes.txt");
        archivo.escribirTextoArchivo("clientes.txt", texto
                + cliente.getNombre()
                + ";" + cliente.getCedula()
                + ";" + cliente.getTelefono());
    }
/**
 * carga los clientes del txt
 * 
 */
    public void cargarClientes() {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String datos = archivo.leerTextoArchivo("clientes.txt");
        String[] aux = datos.split("\n");
        if (!datos.trim().equals("")) {
            clientes = new LinkedList<>();
            for (String aux1 : aux) {
                String[] cl = aux1.split(";");
                Cliente c = new Cliente();
                c.setNombre(cl[0]);
                c.setIdentificacion(cl[1]);
                c.setTelefono(cl[2]);
                clientes.add(c);
            }

        } else {

            clientes = new LinkedList<>();

        }
    }
/**
 * elimina los clientes del txt
 * @param identificacion 
 */
    public void eliminarCliente(String identificacion) {
        String listaClientes = "";
        for (int i = 0; i < clientes.size(); i++) {
            if (!clientes.get(i).getIdentificacion().equals(identificacion)) {
                listaClientes += clientes.get(i).getNombre() + ";"
                        + clientes.get(i).getIdentificacion() + ";"
                        + clientes.get(i).getTelefono() + "\n";
            }
        }
        ManejadorArchivos archivo = new ManejadorArchivos();
        archivo.escribirTextoArchivo("clientes.txt", listaClientes);
        cargarClientes();
    }
/**
 * modifica los clientes en el txt
 * @param identificacion
 * @param cliente 
 */
    public void modificarCliente(String identificacion, Cliente cliente) {
        eliminarCliente(identificacion);
        procesarCliente(cliente);
    }

    public LinkedList<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(LinkedList<Cliente> clientes) {
        this.clientes = clientes;
    }
/**
 * Constantemente el metodo cargar clientes para que se actualicen de inmediato al crear un registro nuevo
 */
    public void refrescar() {
        timer = new Timer();
        TimerTask tarea = new TimerTask() {

            @Override
            public void run() {
                ManejadorArchivos archivo = new ManejadorArchivos();
                String datos = archivo.leerTextoArchivo("clientes.txt");
                String[] aux = datos.split("\n");
                if (!datos.trim().equals("")) {
                    clientes = new LinkedList<>();
                    for (int i = 0; i < aux.length; i++) {
                        String[] cl = aux[i].split(";");
                        Cliente c = new Cliente();

                        c.setNombre(cl[0]);
                        c.setIdentificacion(cl[1]);
                        c.setTelefono(cl[2]);
                        clientes.add(c);
                    }

                } else {
                    clientes = new LinkedList<>();

                }
            }
        };
        timer.schedule(tarea, 0, 1000);
    }

}
