/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CatalogosCRUD;

import bestbuy.Articulo;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import login.ManejadorArchivos;

/**
 *
 * @author JUAN-PC
 */
public final class LogicaArticulo {
    Timer timer;
    private LinkedList<Articulo> articulos;

    public LogicaArticulo(LinkedList<Articulo> articulos) {
        this.articulos = articulos;
    }

    public LogicaArticulo() {
        articulos = new LinkedList<>();
        cargarArticulos();
        refrescar();
    }
/**
 * agrega los articulos a un txt
 * @param articulos 
 */
    public void procesarArticulo(Articulo articulos) {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String texto = archivo.leerTextoArchivo("articulos.txt");
        archivo.escribirTextoArchivo("articulos.txt", texto
                + articulos.getNombre()
                + ";" + articulos.getCodigo()
                + ";" + articulos.getDescripcion()
                + ";" + articulos.getPrecio()
                + ";" + articulos.getBodegaOrigen()
                + ";" + articulos.getCodigoBodega()
                + ";" + articulos.getExistencias());
           
    }
/**
 * carga los articulos del txt
 */
    public void cargarArticulos() {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String datos = archivo.leerTextoArchivo("articulos.txt");
        String[] aux = datos.split("\n");
        if (!datos.trim().equals("")) {
            articulos = new LinkedList<>();
            for (String aux1 : aux) {
                String[] cl = aux1.split(";");
                Articulo c = new Articulo();
                c.setNombre(cl[0]);
                c.setCodigo(cl[1]);
                c.setDescripcion(cl[2]);
                c.setPrecio(Double.parseDouble(cl[3]));
                c.setBodegaOrigen(cl[4]);
                c.setCodigoBodega(cl[5]);
                c.setExistencias(Integer.parseInt(cl[6]));
                articulos.add(c);
            }

        } else {

            articulos = new LinkedList<>();

        }
    }
/**
 * elimina los articulos del txt
 * @param codigo 
 */
    public void eliminarArticulo(String codigo) {
        String listaArticulos = "";
        for (int i = 0; i < articulos.size(); i++) {
            if (!articulos.get(i).getCodigo().equals(codigo)) {
                listaArticulos += articulos.get(i).getNombre() + ";"
                        + articulos.get(i).getCodigo() + ";"
                        + articulos.get(i).getDescripcion() + ";"
                        + articulos.get(i).getPrecio()   + ";"
                        + articulos.get(i).getBodegaOrigen() + ";"
                        + articulos.get(i).getCodigoBodega() +  ";"
                        + articulos.get(i).getExistencias()+ "\n";
            }
        }
        ManejadorArchivos archivo = new ManejadorArchivos();
        archivo.escribirTextoArchivo("articulos.txt", listaArticulos);
        cargarArticulos();
    }
/**
 * modifica un articulo del txt
 * @param codigo
 * @param articulo 
 */
    public void modificarArticulo(String codigo, Articulo articulo) {
        eliminarArticulo(codigo);
        procesarArticulo(articulo);
    }

    public LinkedList<Articulo> getArticulos() {
        return articulos;
    }

    public void setArticulos(LinkedList<Articulo> articulos) {
        this.articulos = articulos;
    }
/**
 * Constantemente el metodo cargar articulos para que se actualicen de inmediato al crear un registro nuevo
 */
    public void refrescar() {
        timer = new Timer();
        TimerTask tarea = new TimerTask() {

            @Override
            public void run() {
                ManejadorArchivos archivo = new ManejadorArchivos();
                String datos = archivo.leerTextoArchivo("articulos.txt");
                String[] aux = datos.split("\n");
                if (!datos.trim().equals("")) {
                    articulos = new LinkedList<>();
                    for (int i = 0; i < aux.length; i++) {
                        String[] cl = aux[i].split(";");
                        Articulo c = new Articulo();
                        c.setNombre(cl[0]);
                        c.setCodigo(cl[1]);
                        c.setDescripcion(cl[2]);
                        c.setPrecio(Double.parseDouble(cl[3]));
                        c.setBodegaOrigen(cl[4]);
                        c.setCodigoBodega(cl[5]);
                        c.setExistencias(Integer.parseInt(cl[6]));
                        articulos.add(c);
                    }

                } else {
                    articulos = new LinkedList<>();

                }
            }
        };
        timer.schedule(tarea, 0, 1000);
    }
    /**
     * disminuye las existencias de articulos 
     * @param articulo
     * @param cantidad 
     */
       public void disminuirExistencia(Articulo articulo,int cantidad) {     
           int canti =0;
           for (int i = 0; i < articulos.size(); i++) {
              if(articulo.getCodigo().equals(articulos.get(i).getCodigo())){
                  canti =cantidad-articulos.get(i).getExistencias(); 
                  eliminarArticulo(articulo.getCodigo());
                  ManejadorArchivos archivo = new ManejadorArchivos();
                  String texto = archivo.leerTextoArchivo("articulos.txt");
                  archivo.escribirTextoArchivo("articulos.txt", texto
                + articulo.getNombre()
                + ";" + articulo.getCodigo()
                + ";" + articulo.getDescripcion()
                + ";" + articulo.getPrecio()
                + ";" + articulo.getBodegaOrigen()
                + ";" + articulo.getCodigoBodega()
                + ";" + canti);
                 
               cargarArticulos();
              }
              
           }
         
    }

}
