/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CatalogosCRUD;

import bestbuy.*;
import java.util.LinkedList;
import login.ManejadorArchivos;

/**
 *
 * @author JUAN-PC
 */
public class LogicaFactura {

    LogicaArticulo loAr = new LogicaArticulo();
    private LinkedList<Articulo> articulosCliente;

    public LogicaFactura(LinkedList<Articulo> articulosCliente) {
        this.articulosCliente = articulosCliente;
    }

    public LogicaFactura() {
    }
 /**
  * busca un articulo del txt por codigo
  * @param codigo
  * @return 
  */
    public Articulo buscarArticulo(String codigo) {
        LinkedList<Articulo> articulos = loAr.getArticulos();
        for (int i = 0; i < loAr.getArticulos().size(); i++) {
            Articulo temp = articulos.get(i);
            if (temp.getCodigo().equals(codigo)) {
                System.out.println(temp.getNombre());
                return temp;

            }
        }
        return null;
    }
/**
 * agrega los pedidos de un cliente a un txt
 * @param cantidad 
 */
    public void procesarPedidoCliente(Articulo ar, int cantidad) {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String texto = archivo.leerTextoArchivo("articulosFactura.txt");
        archivo.escribirTextoArchivo("articulosFactura.txt", texto
                + ar.getNombre()
                + ";" + ar.getCodigo()
                + ";" + ar.getDescripcion()
                + ";" + ar.getPrecio()
                + ";" + ar.getBodegaOrigen()
                + ";" + ar.getCodigoBodega()
                + ";" + cantidad);
    }
/**
 * carga los articulos de factura del txt
 */
    public void cargarArticulosFactura() {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String datos = archivo.leerTextoArchivo("articulosFactura.txt");
        String[] aux = datos.split("\n");
        if (!datos.trim().equals("")) {
            articulosCliente = new LinkedList<>();
            for (String aux1 : aux) {
                String[] cl = aux1.split(";");
                Articulo c = new Articulo();
                c.setNombre(cl[0]);
                c.setCodigo(cl[1]);
                c.setDescripcion(cl[2]);
                c.setPrecio(Double.parseDouble(cl[3]));
                c.setBodegaOrigen(cl[4]);
                c.setCodigoBodega(cl[5]);
                c.setCantidad(Integer.parseInt(cl[6]));
                articulosCliente.add(c);
            }

        } else {

            articulosCliente = new LinkedList<>();

        }
    }
/**
 * calcula los impuestos de los articulos del cliente
 * @param cantidad
 * @param precio
 * @return impuestos
 */
    public double calcularMontoImpuestos(int cantidad, double precio) {
        //System.out.println("");
        double total;
        return total = precio+(precio*0.13);
    }
/**
 * calcula el monto total de la factura del cliente
 * @param cantidad
 * @param precio
 * @return monto total
 */
    public double calcularMontoTotal(int cantidad,double precio) {

        if (cantidad != 1) {

            return calcularMontoImpuestos(cantidad,precio) * cantidad;
        } else {

            return calcularMontoImpuestos(cantidad, precio);

        }
    }
/**
 * es para agregar varios articulos a la factura y calcular el monto total
 * @param articulo
 * @param cantidad 
 */
    public void agregarDetalle(Articulo articulo, int cantidad) {

        for (int i = 0; i <= cantidad; i++) {

            int res = (int) (articulo.getPrecio() * 0.13);
        }

    }

/** 
 * imprime la factura del cliente
 * @param artiFac
 * @param nombreCliente 
 */
    public void ImprimirFactura(LinkedList<Articulo> artiFac, String nombreCliente) {   
     int total=0;
      System.out.println(" Super Mercado The best Buy\n cliente: "+nombreCliente);
        for (Articulo temp : artiFac) {
           calcularMontoImpuestos(temp.getCantidad(), temp.getPrecio());     
              System.out.println(" Producto: "+ temp.getNombre()+ " Precio: "+temp.getPrecio() 
                      +" Monto + Impuestos "+calcularMontoImpuestos(temp.getCantidad()
                     , temp.getPrecio())+" subTotal: "+calcularMontoTotal(temp.getCantidad(), temp.getPrecio()));
            total +=calcularMontoTotal(temp.getCantidad(), temp.getPrecio());           
        }
        System.out.println(" Monto total: "+total);
    }
    
    /**
     * elimina la factura del rxt
     */
        public void eliminarFactura() {
        String listaArticulos = "";
        ManejadorArchivos archivo = new ManejadorArchivos();
        
        archivo.escribirTextoArchivo("articulosFactura.txt", listaArticulos);
        cargarArticulosFactura();
    }

        
       /**
        * elimina un pedido del cliente a la vez
        * @param codigo 
        */ 
    public void eliminarFactura(String codigo) {
          String listaArticulos = "";
        for (int i = 0; i < articulosCliente.size(); i++) {
            if (articulosCliente.get(i).getNombre().equals(codigo)) {
                listaArticulos += articulosCliente.get(i).getNombre() + ";"
                        + articulosCliente.get(i).getCodigo()+ ";"
                        + articulosCliente.get(i).getDescripcion() + ";" 
                         + articulosCliente.get(i).getPrecio()+ ";"
                         + articulosCliente.get(i).getBodegaOrigen()+ ";"
                         + articulosCliente.get(i).getCodigoBodega()+ ";"
                        + articulosCliente.get(i).getCantidad()+ "\n";
            }
        }
        ManejadorArchivos archivo = new ManejadorArchivos();
        archivo.escribirTextoArchivo("articulosFactura.txt", listaArticulos);
        cargarArticulosFactura();
    }
   
    public LogicaArticulo getLoAr() {
        return loAr;
    }

    public void setLoAr(LogicaArticulo loAr) {
        this.loAr = loAr;
    }

    public LinkedList<Articulo> getArticulosCliente() {
        return articulosCliente;
    }

    public void setArticulosCliente(LinkedList<Articulo> articulosCliente) {
        this.articulosCliente = articulosCliente;
    }
}
