/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CatalogosCRUD;

import bestbuy.*;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import login.ManejadorArchivos;

/**
 *
 * @author JUAN-PC
 */
public final class LogicaAgentes {
    Timer timer;
    private LinkedList<Agente> agentes;

    public LogicaAgentes(LinkedList<Agente> agentes) {
        this.agentes = agentes;
    }

    public LogicaAgentes() {
        agentes = new LinkedList<>();
        cargarAgentes();
        refrescar();
    }
/**
 * agrega los agentes a un txt
 * @param agentes 
 */
    public void procesarAgente(Agente agentes) {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String texto = archivo.leerTextoArchivo("agentesDistribuidoras.txt");
        archivo.escribirTextoArchivo("agentesDistribuidoras.txt", texto
                + agentes.getNombre()
                + ";" + agentes.getCedula()
                + ";" + agentes.getTelefono()
                + ";" + agentes.getCodigoDistribuidora()
                + ";" + agentes.getNombreDistribuidora());

    }
/**
 *carga los agentes del txt
 */
    public void cargarAgentes() {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String datos = archivo.leerTextoArchivo("agentesDistribuidoras.txt");
        String[] aux = datos.split("\n");
        if (!datos.trim().equals("")) {
            agentes = new LinkedList<>();
            for (String aux1 : aux) {
                String[] cl = aux1.split(";");
                Agente c = new Agente();
                c.setNombre(cl[0]);
                c.setIdentificacion(cl[1]);
                c.setTelefono(cl[2]);
                c.setCodigoDistribuidora(cl[3]);
                c.setNombreDistribuidora(cl[4]);
                agentes.add(c);
            }

        } else {

            agentes = new LinkedList<>();

        }
    }
/**
 * elimina los agentes del txt
 * @param identificacion 
 */
    public void eliminarAgente(String identificacion) {
        String listaAgentes = "";
        for (int i = 0; i < agentes.size(); i++) {
            if (!agentes.get(i).getIdentificacion().equals(identificacion)) {
                listaAgentes += agentes.get(i).getNombre() + ";"
                        + agentes.get(i).getIdentificacion() + ";"
                        + agentes.get(i).getTelefono() + ";"
                        + agentes.get(i).getCodigoDistribuidora() + ";"
                        + agentes.get(i).getNombreDistribuidora()+"\n";
            }
        }
        ManejadorArchivos archivo = new ManejadorArchivos();
        archivo.escribirTextoArchivo("agentesDistribuidoras.txt", listaAgentes);
        cargarAgentes();
    }
/**
 * modifica los agentes del txt
 * @param identificacion
 * @param agentes 
 */
    public void modificarAgente(String identificacion, Agente agentes) {
        eliminarAgente(identificacion);
        procesarAgente(agentes);
    }
  
    public LinkedList<Agente> getAgentes() {
        return agentes;
    }

    public void setAgentes(LinkedList<Agente> agentes) {
        this.agentes = agentes;
    }
/**
 * Constantemente el metodo cargar agentes para que se actualicen de inmediato al crear un registro nuevo
 */
    public void refrescar() {
        timer = new Timer();
        TimerTask tarea = new TimerTask() {

            @Override
            public void run() {
                ManejadorArchivos archivo = new ManejadorArchivos();
                String datos = archivo.leerTextoArchivo("agentesDistribuidoras.txt");
                String[] aux = datos.split("\n");
                if (!datos.trim().equals("")) {
                    agentes = new LinkedList<>();
                    for (int i = 0; i < aux.length; i++) {
                        String[] cl = aux[i].split(";");
                        Agente c = new Agente();

                        c.setNombre(cl[0]);
                        c.setIdentificacion(cl[1]);
                        c.setTelefono(cl[2]);
                        c.setCodigoDistribuidora(cl[3]);
                        c.setNombreDistribuidora(cl[4]);
                        agentes.add(c);
                    }

                } else {
                    agentes = new LinkedList<>();

                }
            }
        };
        timer.schedule(tarea, 0, 1000);
    }
 /**
  * carga los agentes enlazados con las distribuidoras
  * @param distribuidora 
  */
     public void cargarAgenteDistri(String distribuidora) {
        ManejadorArchivos archivo = new ManejadorArchivos();
        String datos = archivo.leerTextoArchivo("agentesDistribuidoras.txt");
        String[] aux = datos.split("\n");
         LinkedList<Agente> agentess =getAgentes();
        if (!datos.trim().equals("")) {
            agentes = new LinkedList<>();
            int i =agentes.size();
            if (distribuidora.equals(agentes.get(i))) {
               for (String aux1 : aux) {
                String[] cl = aux1.split(";");
                Agente c = new Agente();
                c.setNombre(cl[0]);
                c.setIdentificacion(cl[1]);
                c.setTelefono(cl[2]);
                c.setCodigoDistribuidora(cl[3]);
                c.setNombreDistribuidora(cl[4]);
                agentes.add(c);
            }  
            }
           

        } else {

            agentes = new LinkedList<>();

        }
    }
}
